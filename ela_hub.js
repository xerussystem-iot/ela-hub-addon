/*
  Copyright (c) 2010 - 2017, Nordic Semiconductor ASA
  All rights reserved.
  Redistribution and use in source and binary forms, with or without modification,
  are permitted provided that the following conditions are met:
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer.
  2. Redistributions in binary form, except as embedded into a Nordic
     Semiconductor ASA integrated circuit in a product or a software update for
     such product, must reproduce the above copyright notice, this list of
     conditions and the following disclaimer in the documentation and/or other
     materials provided with the distribution.
  3. Neither the name of Nordic Semiconductor ASA nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.
  4. This software, with or without modification, must only be used with a
     Nordic Semiconductor ASA integrated circuit.
  5. Any software provided in binary form under this license must not be reverse
     engineered, decompiled, modified and/or disassembled.
  THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
  OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

'use strict';

const {
  Adapter,
  Device,
  Property,
  Database	
} = require('gateway-addon');

var xdb =  require('./xerusdb');
const manifest = require('./manifest.json');
var Request =  require("request"); 
const NetcatClient = require('netcat/client')
const fs = require('fs')
var _ = require('underscore');
var result;
var addresses = ['00:04:3E:4E:E2:7D','00:04:3E:4E:E2:7C', '00:04:3E:4E:E2:7E'];				//'00:04:3E:4E:E4:08'; 		
var address = '00:04:3E:4E:E2:7D';
var channel = 1;
var Adaptname = 'ElaDevice';
var self;
var macID = '00:04:3E:4E:E2:7D';

const USER = {
  email: 'sriharsha.desai@xerussystems.com',
  name: 'Sriharsha Desai',
  password: 'bridge@123#',
};

var sensor_count = new Array();
var OnlineStatus = true;
var bridge = new xdb();	  
let bridgeUrl = new Array();  //=["http://192.168.1.144:8080"]; //,"http://192.168.1.200:8080"];
var deviceConfig = new Array();

class ElaDevice extends Device {
  constructor(adapter,URL,thing,jwt) {
    var adapterName = thing.id.substring(thing.id.lastIndexOf("/") + 1).replace(/%3A/g,":");	  
    super(adapter, `${adapterName}`);
    self = this;
    this.token = jwt;
    this.sensorID = thing.id.substring(thing.id.lastIndexOf("/") + 1);	  
    this.tag = thing.title;
    this['@context'] = thing['@context'];
    this['@type'] = thing['@type'];
    this.name = //thing.title; //this.id;
    this.thingurl = URL;	  
    this.description = thing.description;
    this.setProperties(thing.properties);
    //console.log(adapterName);	  
    for(var index = 0; index < adapter.config.sensor.length ; index++) {	  
	console.log(adapter.config.sensor[index].id);    
	if(adapter.config.sensor[index].id === adapterName) {    
	   this.name = adapter.config.sensor[index].name; //thing.title; //this.id;	
	   console.log(this.name);
	   console.log(adapter.config.sensor[index].name);	
	   console.log(adapterName);	
           this.startPolling(adapter.config.sensor[index].pollInterval);
	   	
           break;		
	}	
    }	    
  }

  setProperties(properties) {
	  
     var keys = new Array();
     keys = Object.keys(properties);	
     let Properties = new Object();
     for(var index = 0; index < keys.length; index++) {
        Properties = this.fillProperties(properties[keys[index]]);	  
        this.properties.set(
		keys[index],
		new Property(
		    this,
		    keys[index],
		    Properties,
		    null	
                )	
	);	
     }
  }

  fillProperties(properties) {
      var Pro = new Object();
      var Keys = new Array();
      Keys = Object.keys(properties);
      for( var index = 0 ; index < Keys.length; index++) {
           if(Keys[index] != 'links') {
	      if(Keys[index] === 'title') {
		 Pro['label'] = properties[Keys[index]];     
	      } 
	      else {	      
                 Pro[Keys[index]] = properties[Keys[index]];
	      }	      
           }
      }
      return Pro;
  }

  addProperty(description) {
//    console.log(`adding ${description.title} property`);
    const property = new Property(this, description.title, description);
    this.properties.set(description.title, property);
  }

  generateRandomNumber() {
    return (Math.floor(Math.random() * 100)).toString();
}

  startPolling(interval) {

    var data, tdata, hdata;
    var id = this.sensorID;
    var token = this.token;	   
    this.timer = setInterval(() => {
      data = (Math.floor(Math.random() * 10)).toString()	//random value in range:0-10
      tdata = 18 + Math.round(Math.random()*12);  		//random value in range:18-30
      hdata = (Math.floor(Math.random() * 100)).toString();  	//random value in range:0-100
      get_property(this,this.thingurl,id,'humidity',token);
      get_property(this,this.thingurl,id,'temperature',token);
    }, interval * 1000);

}

  updateValue(name, value) {
    const property = this.properties.get(name);
    property.setCachedValue(parseFloat(value));
    this.notifyPropertyChanged(property);
  }

}

async function get_property(device,URL,id,property,token) {
    var test = "Bearer " + token;
    var url = URL+"/things/"+id+"/properties/"+property;	
   
    const res = await Request.get({
         "headers": { "Authorization": test  ,
		      "content-type": "application/json",
                      "Accept": "application/json" },
         "url": url,
 
    }, (error, response, body) => {
        if(error) {
	   OnlineStatus = false;	
           device.connectedNotify(false);
	   device.updateValue(property,0);	
           return console.dir(error);
        } else {
           device.connectedNotify(true);
	}
	var data = body.split(":")[1];
	if( data != undefined) {     
	  //console.log(data);
	  data = data.replace("}",""); 	
	  //console.log(data);
	  device.updateValue(property,data);    
	} 
    });
   	
   return;  

}	

function checkconfigupdate(adapter,URL,jwt) {
    var configCount = 0;
     var timer = setInterval(() => {
        updateEla(adapter,URL,jwt);               
    },20 * 1000);

}

async function updateEla(adapter,URL,jwt) {

    var setting,things;	
    await xerusGetSetting(URL,jwt,function(err, body) {
       if (err) {
           console.log(err);
       } else {
           setting = body;
       }
    });
    await delay(100);

    let obj = sensor_count.find(o => o.url === URL);	
    //console.log(obj.settings);
    //console.log(setting);	
    if(setting != undefined) { 
	var difference = diff(obj.settings.sensor,setting.sensor);
        //console.log("difference",difference);
	//console.log(difference.length);    
	if(difference.length > 0){
              await xerusThings(URL,jwt,function(err, body) {
                 if (err) {
                  console.log(err);
                 } else {
                  things = body;
                 }
              });
             await delay(100);
	     //console.log(things);
	     if(things != undefined) {	
	         addSetting(adapter,URL,setting);	
	         for(var index = 0; index < things.length; index++) {
	            var id = things[index].id.substring(things[index].id.lastIndexOf("/") + 1).replace(/%3A/g,":");		 
		    console.log("test2",id);	 
                    for( var j = 0; j < difference.length; j++) { 
		       console.log("test1",difference[j]['id']);
		       if(id === difference[j]['id']) {	 
                           const device = new ElaDevice(adapter,URL,
                                          things[index],jwt);
                            adapter.handleDeviceAdded(device);
                       }  
		    }	 
	        }
		
	    }	     
	}
	    
    }	    
}


function comparer(diffArray){
  return function(current){
    return diffArray.filter(function(diff){
      return diff.id == current.id; 
    }).length == 0;
  }
}

function diff(array1,array2){
  var InA1 = array1.filter(comparer(array2));
  var InA2 = array2.filter(comparer(array1));
  return InA2;	
}	


const delay = (duration) =>
  new Promise(resolve => setTimeout(resolve, duration))

async function setup_ela(adapter,URL) {
  let jwt,things,setting; 
  await xerusBridgeConnect(URL, function(err, body) {
       if (err) {
           console.log(err);
       } else {
           jwt = body; 
       }
  });	
 await delay(100);	
 await xerusThings(URL,jwt,function(err, body) {
       if (err) {
           console.log(err);
       } else {
           things = body;
       }
  });
 await delay(100)
 await xerusGetSetting(URL,jwt,function(err, body) {
       if (err) {
           console.log(err);
       } else {
           setting = body;
       }
  });
 await delay(100);
 addSetting(adapter,URL,setting);
 createDevice(adapter,things,URL,jwt);
 checkconfigupdate(adapter,URL,jwt);
}


async function xerusBridgeConnect(URL,callback) {
    var url = URL+"/login";
    	
    const res = await Request.post({
         "headers": { "content-type": "application/json",
                      "Accept": "application/json" },
         "url" : url,
         "body" : JSON.stringify(USER)
    }, (error, response, body) => {
        if(error) {
	   return callback(error || {statusCode: response.statusCode});	
        }
        var data = JSON.parse(response.body);   
	callback(null, data.jwt);        
    });
}

async function xerusThings(URL,jwt,callback) {
    var test = "Bearer " + jwt;
    var url = URL+"/things";
    const res = await Request.get({
         "headers": { "Authorization": test  ,
                      "content-type": "application/json",
                      "Accept": "application/json" },
         "url": url,
 
    }, (error, response, body) => {
        if(error) {
          return callback(error || {statusCode: response.statusCode}); 
        }
	 callback(null, JSON.parse(body));
    });
        
}

async function xerusGetSetting(URL,jwt,callback) {
   let myObj;
    var test = "Bearer " + jwt;
    var url = URL+"/addons/"+manifest.id+"/config";
    const res = await Request.get({
         "headers": { "Authorization": test  ,
                      "content-type": "application/json",
                      "Accept": "application/json" },
         "url": url,

    }, (error, response, body) => {
        if(error) {
           return callback(error || {statusCode: response.statusCode});
        }
	var config = new Object();    
	config = JSON.parse(body);
	callback(null, config);    
    });
	
}

function addAdapterConfig(config,value) {
      const found = config.some(el => el.id === value.id);
  if (!found) config.push(value);
}


function addSetting(adapter,URL,setting) {
   //console.log(setting);	
   if(setting.sensor != undefined && setting.sensor.length != 0) {    
        var count  = adapter.config.sensor.length; 
	var sen_count = {"url":URL,"settings":setting};
	//console.log("sen_count",JSON.stringify(sen_count));  
	 /*if url not found push it to sensor_count, if found update the sensor_count*/  
	if(sensor_count.find(x => x.url === URL) === undefined) {
	   console.log("add new entry");	
	   sensor_count.push(sen_count);   
	} else {
	   console.log("update the value");
 	   var temp = [sen_count];
	   //console.log(sensor_count[0].url);	
	   var res = sensor_count.map(obj => temp.find(o => o.url === obj.url) || obj);
	   //console.log(res);
	   sensor_count = res;	
	}
	//console.log(JSON.stringify(sensor_count));   
        for(var index = 0; index < setting.sensor.length; index++) {
            addAdapterConfig(adapter.config.sensor,setting.sensor[index]);                
        }       
        adapter.db.saveConfig(adapter.config);
   }
   console.log("---------------------------------------");
   console.log(adapter.config);   
   console.log(adapter.config.sensor.length);   
   console.log("---------------------------------------");	
}	

function createDevice(adapter,things,URL,jwt) {
   let myObj;	
   for(var index = 0; index < things.length; index++) {    
       var sensorClass = things[index].id
       //console.log(sensorClass);     
       //console.log(things);	   
       if(sensorClass.includes(Adaptname)) {
          myObj = things[index];     
          //console.log("INDEX:"+index); 
          //console.log(adapter.config.sensor[index].pollInterval); 
          const device = new ElaDevice(adapter,URL,
                                           myObj,jwt);
	  console.log(device.asThing());     
          adapter.handleDeviceAdded(device);
       }           
   }
}

class ElaAdapter extends Adapter {
  constructor(addonManager, config, db) {
    super(addonManager, ElaAdapter.name, manifest.id);
    this.config = config;
    this.db = db;
    const pollInterval = this.config.pollInterval;
    addonManager.addAdapter(this);
     const knownDevices = {};
     const knownDevice = knownDevices[address];
     bridge.open();
     kickOff(this);	  
   }

}

var brigde_scan_intertval = 3;
var ping_interval = 3;

function scan_host(obj,host) {
   var timer = setInterval(() => {
      var nc = new NetcatClient()
      var hostName = host.split("//")[1].split(":")[0];
      nc.addr(hostName).scan('8080-8080', function(port){
         if(port['8080'] === 'open') {
	    setup_ela(obj,host); 
            clearInterval(timer);
         }
      })
  }, ping_interval * 1000);
}

var line_read = 0;

function kickOff(obj) {
   var timer = setInterval(() => {	
       new Promise(function(resolve, reject) {
           setTimeout(function() {
              resolve(bridge.loadBridgeDb());
           }, 1000);
       }).then(function(value) {
          if(line_read < value.length) { 	  
             for(var index = line_read; index < value.length ; index++) {
	         scan_host(obj,value[index]);
             }
	     line_read = value.length;	  
          }
       });
    }, brigde_scan_intertval * 1000);	   
}

module.exports = ElaAdapter;

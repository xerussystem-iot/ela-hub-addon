'use strict';

const {Database} = require('gateway-addon');
const manifest = require('./manifest.json');
const ElaAdapter = require('./ela_hub');

module.exports = (addonManager, _, errorCallback) => {
  const db = new Database(manifest.id);
  db.open().then(() => {
    return db.loadConfig();
  }).then((config) => {
    new ElaAdapter(addonManager, config ,db);
  }).catch((e) => {
    console.log("error");
    errorCallback(manifest.id, e);
  });
};

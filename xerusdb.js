/**
 * Wrapper around the gateway's database.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

'use strict';

const fs = require('fs');
const os = require('os');
const path = require('path');
const sqlite3 = require('sqlite3').verbose();

const DB_PATHS = [
  path.join(os.homedir(), 'mozilla-iot', 'gateway', 'configurator.sqlite3'),
  path.join(os.homedir(), '.mozilla-iot', 'config', 'configurator.sqlite3'),
  path.join('.', 'configurator.sqlite3'),               // if cwd is the gateway dir
  path.join('..', '..', '..', 'configurator.sqlite3'),  // if cwd is the add-on dir
];

if (process.env.hasOwnProperty('MOZIOT_HOME')) {
  DB_PATHS.unshift(path.join(process.env.MOZIOT_HOME, 'config', 'configurator.sqlite3'));
}

if (process.env.hasOwnProperty('MOZIOT_DATABASE')) {
  DB_PATHS.unshift(process.env.MOZIOT_DATABASE);
}

/**
 * An Action represents an individual action on a device.
 */
class xerusDatabase {
  /**
   * Initialize the object.
   *
   * @param {String} packageName The adapter's package name
   * @param {String?} path Optional database path
   */
  constructor(packageName, path = null) {
    this.packageName = packageName;
    this.path = path;
    this.conn = null;

    if (!this.path) {
      for (const p of DB_PATHS) {
        if (fs.existsSync(p)) {
          this.path = p;
          break;
        }
      }
    }
  }

  /**
   * Open the database.
   *
   * @returns Promise which resolves when the database has been opened.
   */
  open() {
    if (this.conn) {
      return Promise.resolve();
    }

    if (!this.path) {
      return Promise.reject(new Error('Database path unknown'));
    }

    return new Promise((resolve, reject) => {
      this.conn = new sqlite3.Database(
        this.path,
        (err) => {
          if (err) {
            reject(err);
          } else {
            this.conn.configure('busyTimeout', 10000);
            resolve();
          }
        });
    });
  }

  /**
   * Close the database.
   */
  close() {
    if (this.conn) {
      this.conn.close();
      this.conn = null;
    }
  }

  /**
   * Load the package's config from the database.
   *
   * @returns Promise which resolves to the config object.
   */
  loadBridgeDb() {
    if (!this.conn) {
      return Promise.reject('Database not open');
    }

    return new Promise((resolve, reject) => {
      this.conn.all(
        'SELECT url FROM bridge_data',
         (error, rows) => {
          if (error) {
            reject(error);
          } else if (!rows) {
            resolve({});
          } else {
	    const hosturl = new Array();	  
	    for (const row of rows) {
               var url = row.url;
	       hosturl.push(url); 	    
	    }	    
	    resolve(hosturl);
          }
        });
    });
  }

}

module.exports = xerusDatabase;
